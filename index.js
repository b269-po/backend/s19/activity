/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/


let logInUser = prompt("Enter your Username:");
let logInPass = prompt("Enter your Password:");
let logInRole = prompt("Enter your Role:");

console.log(logInUser);
console.log(logInPass);
console.log(logInRole);

if(logInUser === "" || logInUser === null) {
	alert("Please enter your username!");
}
if(logInPass === "" || logInPass === null) {
	alert("Please enter your password!");
} 
if(logInRole === "" || logInRole === null) {
	alert("Please enter your role!");
} else {
	switch(logInRole){
	case "admin":
		alert("Welcome back to the class portal, admin!");
		break;
	case "teacher":
		alert("Thank you for logging in, teacher!");
		break;
	case "student":
		alert("Welcome to the class portal, student!");
		break;
	default:
		alert("Role out of range.");
		break;
	}
}

function determineGrade(score1, score2, score3, score4) {
	let average = (score1 + score2 + score3 + score4) / 4;
	console.log("checkAverage(71, 70, 73, 74)");
	let average1 = Math.round(average);
	

	if (average1 < 74) {
		console.log("Hello, student, your average is " + average1 + " . The letter equivalent is F");
	} else if (average1 >= 75 && average1 <=79) {
		console.log("Hello, student, your average is " + average1 + " . The letter equivalent is D");
	} else if (average1 >= 80 && average1 <=84) {
		console.log("Hello, student, your average is " + average1 + " . The letter equivalent is C");
	} else if (average1 >= 85 && average1 <=89) {
		console.log("Hello, student, your average is " + average1 + " . The letter equivalent is B");
	} else if (average1 >= 90 && average1 <=95) {
		console.log("Hello, student, your average is " + average1 + " . The letter equivalent is A");
	} else if (average1 >= 96) {
		console.log("Hello, student, your average is " + average1 + " . The letter equivalent is A+");
	} 

}

let variableAverage = determineGrade(71, 70, 73, 74);
console.log(variableAverage);

function determineGrade(score5, score6, score7, score8) {
	let average = (score5 + score6 + score7 + score8) / 4;
	console.log("checkAverage(75, 75, 76, 78)");
	let average2 = Math.round(average);
	

	if (average2 < 74) {
		console.log("Hello, student, your average is " + average2 + " . The letter equivalent is F");
	} else if (average2 >= 75 && average2 <=79) {
		console.log("Hello, student, your average is " + average2 + " . The letter equivalent is D");
	} else if (average2 >= 80 && average2 <=84) {
		console.log("Hello, student, your average is " + average2 + " . The letter equivalent is C");
	} else if (average2 >= 85 && average2 <=89) {
		console.log("Hello, student, your average is " + average2 + " . The letter equivalent is B");
	} else if (average2 >= 90 && average2 <=95) {
		console.log("Hello, student, your average is " + average2 + " . The letter equivalent console.log(s A");
	} else if (average2 >= 96) {
		console.log("Hello, student, your average is " + average2 + " . The letter equivalent is A+");
	} 

}

let variableAverage1 = determineGrade(75, 75, 76, 78);
console.log(variableAverage1);

function determineGrade(score5, score6, score7, score8) {
	let average = (score5 + score6 + score7 + score8) / 4;
	console.log("checkAverage(80, 81, 82, 78)");
	let average3 = Math.round(average);
	

	if (average3 < 74) {
		console.log("Hello, student, your average is " + average3 + " . The letter equivalent is F");
	} else if (average3 >= 75 && average3 <=79) {
		console.log("Hello, student, your average is " + average3 + " . The letter equivalent is D");
	} else if (average3 >= 80 && average3 <=84) {
		console.log("Hello, student, your average is " + average3 + " . The letter equivalent is C");
	} else if (average3 >= 85 && average3 <=89) {
		console.log("Hello, student, your average is " + average3 + " . The letter equivalent is B");
	} else if (average3 >= 90 && average3 <=95) {
		console.log("Hello, student, your average is " + average3 + " . The letter equivalent console.log(s A");
	} else if (average3 >= 96) {
		console.log("Hello, student, your average is " + average3 + " . The letter equivalent is A+");
	} 

}

let variableAverage2 = determineGrade(80, 81, 82, 78);
console.log(variableAverage2);

function determineGrade(score5, score6, score7, score8) {
	let average = (score5 + score6 + score7 + score8) / 4;
	console.log("checkAverage(84, 85, 87, 88)");
	let average4 = Math.round(average);
	

	if (average4 < 74) {
		console.log("Hello, student, your average is " + average4 + " . The letter equivalent is F");
	} else if (average4 >= 75 && average4 <=79) {
		console.log("Hello, student, your average is " + average4 + " . The letter equivalent is D");
	} else if (average4 >= 80 && average4 <=84) {
		console.log("Hello, student, your average is " + average4 + " . The letter equivalent is C");
	} else if (average4 >= 85 && average4 <=89) {
		console.log("Hello, student, your average is " + average4 + " . The letter equivalent is B");
	} else if (average4 >= 90 && average4 <=95) {
		console.log("Hello, student, your average is " + average4 + " . The letter equivalent console.log(s A");
	} else if (average4 >= 96) {
		console.log("Hello, student, your average is " + average4 + " . The letter equivalent is A+");
	} 

}

let variableAverage3 = determineGrade(84, 85, 87, 88);
console.log(variableAverage3);


function determineGrade(score5, score6, score7, score8) {
	let average = (score5 + score6 + score7 + score8) / 4;
	console.log("checkAverage(89, 90, 91, 90)");
	let average5 = Math.round(average);
	

	if (average5 < 74) {
		console.log("Hello, student, your average is " + average5 + " . The letter equivalent is F");
	} else if (average5 >= 75 && average5 <=79) {
		console.log("Hello, student, your average is " + average5 + " . The letter equivalent is D");
	} else if (average5 >= 80 && average5 <=84) {
		console.log("Hello, student, your average is " + average5 + " . The letter equivalent is C");
	} else if (average5 >= 85 && average5 <=89) {
		console.log("Hello, student, your average is " + average5 + " . The letter equivalent is B");
	} else if (average5 >= 90 && average5 <=95) {
		console.log("Hello, student, your average is " + average5 + " . The letter equivalent console.log(s A");
	} else if (average5 >= 96) {
		console.log("Hello, student, your average is " + average5 + " . The letter equivalent is A+");
	} 

}

let variableAverage4 = determineGrade(89, 90, 91, 90);
console.log(variableAverage4);


function determineGrade(score5, score6, score7, score8) {
	let average = (score5 + score6 + score7 + score8) / 4;
	console.log("checkAverage(91, 96, 97, 99)");
	let average6 = Math.round(average);
	

	if (average6 < 74) {
		console.log("Hello, student, your average is " + average6 + " . The letter equivalent is F");
	} else if (average6 >= 75 && average6 <=79) {
		console.log("Hello, student, your average is " + average6 + " . The letter equivalent is D");
	} else if (average6 >= 80 && average6 <=84) {
		console.log("Hello, student, your average is " + average6 + " . The letter equivalent is C");
	} else if (average6 >= 85 && average6 <=89) {
		console.log("Hello, student, your average is " + average6 + " . The letter equivalent is B");
	} else if (average6 >= 90 && average6 <=95) {
		console.log("Hello, student, your average is " + average6 + " . The letter equivalent console.log(s A");
	} else if (average6 >= 96) {
		console.log("Hello, student, your average is " + average6 + " . The letter equivalent is A+");
	} 

}

let variableAverage5 = determineGrade(91, 96, 97, 99);
console.log(variableAverage5);
